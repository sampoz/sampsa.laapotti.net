# My site

Here is my site, will probably be blogging and creating tutorials for stuff :)

Created with Hugo <3

## To dev locally

```
hugo server
```

## To build and  upload

```
./build-and-upload.sh
```

TODO Refactor to use with terminal theme
remember to generate thumbnails with
```
convert -resize 25% content/post/A-small-fix-for-TI83+/a-fix.JPG content/post/A-small-fix-for-TI83+/a-fix_tb.JPG
```
