+++
date = "2017-06-18"
title = "A small fix for MacBookPro11,x Linux powesave interrupt problems"
draft = false
description = "I found a fix for a powersave problem I have been having with 2015 15 MBP"

+++
I have been living with a 2015 15" retina mbp and using GNU/Linux(Linux Mint 17 atm) in it for few years. The hardware is pretty nice, but drivers and powersaving has been pretty shaky. My average battery life has been around 2 hours. People using OS X usually report battery lifes of about 8 hours with this configuration. So there is something fishy going on with the power saving in Linux.

TL; DR; Disable the most used ACPI-interrupts to save power:

	# Check which interrupts are getting triggered
	grep . -r /sys/firmware/acpi/interrupts/
	# Disable the offending interrupt. In my case it was gpe06
	echo "disable" | sudo bash -c "cat > /sys/firmware/acpi/interrupts/gpe06"

I found this fix by monitoring the power usage with [powertop](https://01.org/powertop/)

If you want to persist this fix over reboots, you can add it to your /etc/rc.local in order to run it after boot.
