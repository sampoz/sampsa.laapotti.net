+++
title = "Data from my thesis published"
date = "2020-11-16T16:03:10+02:00"
author = "Sampsa Laapotti"
authorTwitter = "" #do not include @
cover = ""
tags = ["thesis", "science"]
keywords = ["", ""]
description = "I published my thesis data and data-analysis jupyter notebook for general public"
showFullContent = false
+++

I got my thesis data anonymized and ready to be published for public consumption. Here it is: [dippa-analysis-anonymized.zip](/dippa/dippa-analysis-anonymized.zip)! If you want to run it, please follow instruction in the README.md file inside the zip.

The full thesis can be found [here](/dippa/dippa_final_with_links.pdf). The archival version without fancy linking can be found [here](/dippa/SCI_2020_Laapotti_Sampsa.pdf).

I am forever grateful to all the people who helped me with my studies! You are all awesome 💟