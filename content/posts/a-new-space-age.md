+++
date = "2017-03-31T00:02:34+03:00"
title = "A New Space Age has Dawned"
draft = false
description = "SpaceX has succesfully re-launched and re-landed it's reusable rocket. Why is this important?"

+++

Today, March 31st 2017, SpaceX has successfully re-launched and re-landed it's reusable first stage of the Falcon 9 rocket. Why is this such an important occasion for the humankind?

##  SpaceX can now launch mass to orbit with even steeper discounts.
It seems that about 3/4 of the [costs associated with a falcon 9 launch](http://space.stackexchange.com/questions/8330/what-is-the-cost-breakdown-for-a-falcon-9-launch) are due to the first stage, reusing it will probably bring huge reductions in cost. SpaceX is already a cheap provider, but this could open doors for completely new areas of space exploration and exploitation.

## How many reuses?
Does it really matter? Even a few reuses will cut the costs. There might be some problems with finding customers, as satellites tend to be quite valuable payloads, but if SpaceX can nail the reusability charasteristics ( basically scrap the rockets before failures, or offer the failure prone flights with great discounts ), customers will not be a problem. It would be really cool to see 10 launches on the same core, even a hundred, but probably the first generation will see a lot less burn time. Old rocket-engines used to burn only for about a hundred seconds, so we are in pretty unknown territory with the multi-thousand-second engines that SpaceX will soon start having. On the other hand, most of those engines were designed in the 50's and the 60's and engineering has progressed between those times and the 2010's

## What now?
I falsifiably predict, that this will ignite a new renaissance of space exploration. Launch prices are declining, tehnology is progressing, Our Environment is faltering and The private space frontiers is now open.

What A Time to be Alive!
