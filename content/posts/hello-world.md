+++
date = "2017-03-27T00:01:34+03:00"
title = "Hello World"
draft = false
description = "The first blogpost, motivations behind this endeavour"

+++

Here it is, my very own site <3

## Why build a personal site in the age of social media?

In 2016 I decided to quit Facebook, and while it feels really good, I sometimes long for a platform with which to use in order to store and forward knowledge I have aqcuired, or maybe to muse about philosophy or ethics. And here it is.

Welcome aboard for the ride!
