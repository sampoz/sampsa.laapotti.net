+++
date = "2017-12-01T16:30:00+02:00"
title = "Teardown of Maxtor M3 4TB"
draft = false

+++

I am actively on lookout for cheapest 2.5" drives for my backups and
it seems that cheapest way to get 2.5" sata drives is to buy external
drives and loot the hard disk from it. Sadly some manufacturers don't
use sata drives in their external drives, so one has to be careful
which drive to buy.

An easy way to get a good look on the current disk prices in to check
[diskprices.com](https://diskprices.com/) and that's how I found this
[drive](https://www.amazon.de/gp/product/B01AJWNWWQ/). It is about 0.028 €/GB at the moment, which is pretty good
price for 2.5" drive. Cheapest 3.5" drives are about 0.023 €/GB today
(December 2017).

The drive arrived from Amazon.de with minimal fuss and just the right
amount of accessories (a cable, minimal manual). I proceded to tear it
open:

{{< figure src="/post/Teardown-of-Maxtor-M3-4TB/teardown_tb.JPG" link="/post/Teardown-of-Maxtor-M3-4TB/teardown.JPG" caption="Teardown of the external drive" >}}

Plastic shell is not fastened with screws, so one can just pry it open
with a screwdrive to expose the construction. I was please to see,
that this drive uses a usb3-to-SATA bridge and has a standard
SATA-connector in place. Otherwise the build quality seem adequate.

{{< figure src="/post/Teardown-of-Maxtor-M3-4TB/disk_tb.JPG" link="/post/Teardown-of-Maxtor-M3-4TB/disk.JPG" caption="Closeup on the disk" >}}

The drive is a [Seagate Barracuda
ST4000LM024](https://www.seagate.com/www-content/product-content/seagate-laptop-fam/barracuda_25/en-us/docs/100804767c.pdf)
and seems completely legit. Nice :)

{{< figure src="/post/Teardown-of-Maxtor-M3-4TB/disk_angle_tb.JPG" link="/post/Teardown-of-Maxtor-M3-4TB/disk_angle.JPG" caption="Angled shot showing the SATA-interface present" >}}

As you can see, the SATA-interface is present. This drive is suitable
for my zfs-pool.
