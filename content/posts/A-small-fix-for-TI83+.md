+++
date = "2017-11-11"
title = "A small fix for TI83+ not booting / screen not working"
draft = false
description = "My ti-83+ would not boot, solved with a little bit of solder"

+++
My Trusty Texas instruments TI-83+ could not boot, so I decided to
trace the connectors on the pcb, found that one connector between the
lcd-board and the motherboard had been interrupted. I rerouted it with
a jump-wire and now TI-83+ seems to boot fine :)


{{< figure src="/post/A-small-fix-for-TI83+/a-fix_tb.JPG" link="/post/A-small-fix-for-TI83+/a-fix.JPG" caption="A jumper connecting over broken trace" >}}
