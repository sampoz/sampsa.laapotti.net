---
title: "About"
date: 2020-11-19T09:27:09+02:00
draft: false
---
I am an aspiring rationalist on a quest to lower the suffering in the world and to enjoy the ride. I work as DevOps Engineer at Wolt. This is my personal blog.
