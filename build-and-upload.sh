#! /bin/bash
# Build site with themes
hugo

# Sync publicly
aws s3 sync --acl public-read public/ s3://sampsa.laapotti.net
